#!/bin/bash

export DOCKER=${DOCKER:-"docker"}
export HOST=${HOST:-"$HOME"}

# determine if we're running docker or podman
$DOCKER --help 2>&1 | grep -q podman
if [ $? -ne 0 ]; then
    extra_docker_run_args="--group-add wheel"
else
    extra_docker_run_args="--userns=keep-id --group-add wheel"
fi

set -e
set -x
$DOCKER run --rm -it \
    --hostname in-container \
    $extra_docker_run_args \
    -v ${HOST}:/${HOME} \
    -v $(realpath /etc/localtime):/etc/localtime:ro \
    build-container \
    bash

