# build-container

build-container is a docker container image that includes Merge development
tools. It it based off of a fedora 40 base image.

## prerequisites

You need to install either docker or podman on your laptop (or whatever local development machine
you are using)

## downloading

To use it, run these commands:
```
git clone https://gitlab.com/mergetb/testbeds/sphere/internships/tools.git
cd tools/build-container
```

## configuring local environment

If you are using docker:
```bash
export DOCKER=docker
```

If you are using podman:
```bash
export DOCKER=podman
```

## building

```bash
./build-container.sh
```

## running

The build-container will mount a directory from your local machine into the container. By default,
it will mount your current directory. The `HOST` environment variable can be set to change this;
e.g., to run the container in the context of your home directory:
```bash
export HOST=/home/`whoami`
```

```bash
./run-container.sh
```

Note: this will enter you into the container. The value you set to `HOST` will be mounted to your
HOME directory in the container. This is where your initial working directory will be set.
