#!/bin/bash

export DOCKER=${DOCKER:-"docker"}
export DOCKER_ARGS=${DOCKER_ARGS:-"--no-cache"}

uid=$(id -u)
uname=$(id -un)
gid=$(id -g)
group=$(id -gn) 

$DOCKER build \
    $DOCKER_ARGS \
    -f Dockerfile \
    --build-arg UID="$uid" --build-arg GID="$gid" --build-arg GROUP="$group" --build-arg UNAME="$uname" \
    -t build-container:latest .
